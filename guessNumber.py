import random
def guessNumber():
    randomNumber = random.randint(1, 20)
    userNumber = input("Podaj liczbe od 1 do 20")
    userNumberInt = int(userNumber)
    if (randomNumber == userNumberInt):
        print("Zgadles liczbe " + str(userNumberInt))
    else:
        print("Nie udalo sie odgadnac liczby" +
              ". Prawidlowa liczba to" + str(randomNumber))
guessNumber()